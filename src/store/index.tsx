import { configureStore } from '@reduxjs/toolkit'
import allSlices from './slices'

const store = configureStore({
  reducer: allSlices
})

export default store

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

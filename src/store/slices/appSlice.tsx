import { createSlice } from '@reduxjs/toolkit'

const initialState: { isLoading: boolean, tasks: any[] } = {
  isLoading: false,
  tasks: []
}
const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    setLoading (state) {
      state.isLoading = true
      return state
    },
    resetLoading (state) {
      state.isLoading = false
      return state
    },
    setTasks (state, action) {
      state.tasks = action.payload
      return state
    },
    createNewTask (state, action) {
      const task = {
        id: state.tasks.length + 1,
        rootId: action.payload.rootId,
        description: action.payload.description,
        createdBy: action.payload.userId,
        isDone: false,
        hasSubtasks: false
      }

      const newAllTasks = [
        ...state.tasks,
        task
      ]

      state.tasks = newAllTasks
      return state
    },
    toggleTaskDone (state, action) {
      const {
        id
      } = action.payload

      const { tasks } = state

      const index = tasks.findIndex((x) => x.id === id)
      tasks[index] = {
        ...tasks[index],
        isDone: tasks[index].isDone !== true
      }
      state.tasks = tasks
      return state
    }
  }
})

export const { setLoading, resetLoading, setTasks, createNewTask, toggleTaskDone } = appSlice.actions
export default appSlice.reducer

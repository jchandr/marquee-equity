import { createSlice } from '@reduxjs/toolkit'
const initialState: { isLoggedIn: boolean, email: string, token: string, id: number } = {
  isLoggedIn: false,
  email: '',
  token: '',
  id: -1
}
const appSlice = createSlice({
  name: 'app',
  initialState,
  reducers: {
    login (state, action) {
      const {
        email,
        token,
        id
      } = action.payload
      state.isLoggedIn = true
      state.email = email
      state.token = token
      state.id = id
      return state
    },
    logout (state) {
      state = initialState
      return state
    }
  }
})

export const { login, logout } = appSlice.actions
export default appSlice.reducer

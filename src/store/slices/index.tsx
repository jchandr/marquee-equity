import appSlice from './appSlice'
import userSlice from './userSlice'
const allSlices = {
  app: appSlice,
  user: userSlice
}

export default allSlices

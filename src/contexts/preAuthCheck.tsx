import React, { useEffect, useState } from 'react'
import { Outlet } from 'react-router-dom'
import { AuthProvider, useAuth } from './authContext'
import { useAppDispatch } from '../store/hooks'
import { resetLoading, setLoading } from '../store/slices/appSlice'
import { login } from '../store/slices/userSlice'
import { verifyToken } from '../mockBackend'

const PreAuthCheck = (): any => {
  const [isLastUserValidated, setIsLastUserValidated] = useState(false)
  const dispatchAction = useAppDispatch()
  const { user, login: loginAuthContext, logout } = useAuth()
  const revalidateLastUser = async (): Promise<void> => {
    await new Promise((resolve, reject) => {
      dispatchAction(setLoading())
      if (user === undefined || user === null) {
        logout()
        reject(Error())
        dispatchAction(resetLoading())
        setIsLastUserValidated(true)
      }
      if (user.token !== null || user.token !== undefined) {
        // validate the token here set the user state
        verifyToken(user.token).then((data) => {
          loginAuthContext(user)
          dispatchAction(login(data))
          dispatchAction(resetLoading())
          setIsLastUserValidated(true)
        }).catch(() => {
          logout()
          reject(Error())
          dispatchAction(resetLoading())
          setIsLastUserValidated(true)
        })
      } else {
        logout()
        reject(Error())
        dispatchAction(resetLoading())
        setIsLastUserValidated(true)
      }
    })
  }

  useEffect(() => {
    revalidateLastUser().finally(() => { setIsLastUserValidated(true) }).catch(() => {})
  }, [])

  return (
    <>
        {isLastUserValidated && <AuthProvider>
            <Outlet></Outlet>
            </AuthProvider>}
    </>
  )
}

export default PreAuthCheck

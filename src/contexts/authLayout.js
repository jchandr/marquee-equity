import React from 'react'
import { Outlet } from 'react-router-dom'
import { AuthProvider } from './authContext'

const AuthLayout = () => {
  return (
    <AuthProvider>
      <Outlet></Outlet>
    </AuthProvider>
  )
}

export default AuthLayout

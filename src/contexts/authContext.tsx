import React, {
  type ReactElement,
  createContext, useContext, useMemo
} from 'react'
import { useNavigate } from 'react-router-dom'
import { useLocalStorage } from '../localstorage'

const AuthContext = createContext({})

export function AuthProvider ({ children }: { children: ReactElement }): any {
  const [user, setUser] = useLocalStorage('user', null)
  const navigate = useNavigate()

  const login = (data: object): void => {
    setUser(data)
  }

  const logout = (): void => {
    setUser(null)
    navigate('/login', { replace: true })
  }

  const value = useMemo(
    () => ({
      user,
      login,
      logout
    }),
    [user]
  )
  return (
      <AuthContext.Provider value={value}>
        {children}
      </AuthContext.Provider>
  )
}

export const useAuth = (): any => useContext(AuthContext)

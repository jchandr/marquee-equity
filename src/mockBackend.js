import database from './database.json'

const users = database.users
const tokens = database.tokens
const todos = database.todos

export const _login = (email, password) => new Promise((resolve, reject) => {
  const match = users.find(x => x.email === email)

  if (match !== undefined) {
    if (match.password === password) {
      const token = tokens.find(x => x.userId === match.id)
      if (token === undefined) {
        reject(Error('invalid'))
      }
      if (new Date(token.expires) >= new Date()) {
        const data = {
          email: match.email,
          name: match.name,
          id: match.id,
          token: token.token
        }
        resolve(data)
      }
      reject(Error('invalid'))
    }
  } else {
    reject(Error('invalid'))
  }
})

export const verifyToken = (authToken) => new Promise((resolve, reject) => {
  const token = tokens.find(x => x.token === authToken)

  if (token === undefined) {
    reject(Error('invalid'))
  }

  if (new Date() > new Date(token.expires)) {
    reject(Error('invalid'))
  }

  const match = users.find(x => x.id === token.userId)

  resolve({
    email: match.email,
    name: match.name,
    id: match.id,
    token: token.token
  })
})

export const getTodos = (authToken) => new Promise((resolve, reject) => {
  verifyToken(authToken).then((userData) => {
    const { id: userId } = userData
    const filteredTodos = todos.filter(x => x.createdBy === userId)
    resolve(filteredTodos)
  }).catch(() => {
    reject(Error('invalid'))
  })
})

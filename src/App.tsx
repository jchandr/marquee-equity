import React, { type ReactElement } from 'react'
import Router from './router'

const App = (): ReactElement => {
  return <Router></Router>
}

export default App

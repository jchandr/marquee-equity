import React, { type ReactElement } from 'react'
import { Outlet, Route, RouterProvider, createBrowserRouter, createRoutesFromElements, useLocation, Navigate } from 'react-router-dom'
import Login from '../views/login'
import { useAppDispatch, useAppSelector } from '../store/hooks'
import DashboardIndex from '../views/dashboard'
import AuthLayout from '../contexts/authLayout'
import PreAuthCheck from '../contexts/preAuthCheck'
import { Navbar, Container, Row, Col, Button } from 'react-bootstrap'
import { useAuth } from '../contexts/authContext'
import { logout } from '../store/slices/userSlice'

const ProtectedRoutes = (): ReactElement => {
  const location = useLocation()
  const user = useAppSelector(state => state.user)
  const { logout: logoutAuthContext } = useAuth()
  const dispatchAction = useAppDispatch()
  const handleLogoutClick = (e: any): void => {
    e.preventDefault()
    logoutAuthContext()
    dispatchAction(logout())
  }
  return user.isLoggedIn
    ? (
      <>
      <Navbar expand="lg" className="bg-body-tertiary">
      <Container>
        <Navbar.Text>
        Welcome {user.email}
        </Navbar.Text>
        <Row>
          <Col>
          <Button variant='outline-danger' onClick={(e: any) => { handleLogoutClick(e) }}>Logout</Button>
          </Col>
        </Row>
      </Container>
    </Navbar>
    <Outlet></Outlet>
      </>

      )
    : <Navigate replace state={{ from: location }} to="/login" />
}

const NotFound = (): ReactElement => {
  const user = useAppSelector(state => state.user)

  return user.isLoggedIn ? <Navigate to ='dashboard'></Navigate> : <Navigate to='login'></Navigate>
}

const Router = (): ReactElement => {
  const router = createBrowserRouter(createRoutesFromElements(
        <Route element={<AuthLayout></AuthLayout>}>
            <Route element={<PreAuthCheck></PreAuthCheck>}>
              <Route element={<ProtectedRoutes></ProtectedRoutes>}>
                <Route path='/dashboard' element={<DashboardIndex></DashboardIndex>}></Route>
              </Route>
              <Route index path="login" element={<Login></Login>}></Route>
              <Route path="*" element={<NotFound></NotFound>} />
            </Route>
        </Route>
  ))

  return <RouterProvider router={router}></RouterProvider>
}

export default Router

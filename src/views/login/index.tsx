import React, { useEffect, type ReactElement, useState } from 'react'
import { useAuth } from '../../contexts/authContext'
import { useNavigate } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from '../../store/hooks'
import { login } from '../../store/slices/userSlice'
import { _login } from '../../mockBackend'
import { Alert, Container } from 'react-bootstrap'

const Login = (): ReactElement => {
  const { login: loginAuthContext } = useAuth()
  const user = useAppSelector(state => state.user)
  const navigate = useNavigate()
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [errorMessage, setErrorMessage] = useState('')

  const dispatchAction = useAppDispatch()
  const handleSubmit = (e: any): void => {
    e.preventDefault()
    _login(email, password).then((data) => {
      console.log(data)
      loginAuthContext(data)
      dispatchAction(login(data))
      navigate('/dashboard')
    }).catch((err) => {
      console.log(err)
      setErrorMessage('Invalid Login')
    })
  }

  useEffect(() => {
    if (user.isLoggedIn) {
      navigate('/dashboard')
    }
  }, [])

  return (
    <Container className='mt-4'>
      <form onSubmit={(e) => { handleSubmit(e) }}>
        <h3>Sign In</h3>
        <div className="mb-3">
          <label>Email address</label>
          <input
            type="email"
            value={email}
            onChange={(e) => { setEmail(e.target.value) }}
            className="form-control"
            placeholder="Enter email"
          />
        </div>
        <div className="mb-3">
          <label>Password</label>
          <input
            value={password}
            onChange={e => { setPassword(e.target.value) }}
            type="password"
            className="form-control"
            placeholder="Enter password"
          />
        </div>
        <div className="d-grid">
          {
            errorMessage.length > 0 && <Alert key={'daner'} variant='danger'>{errorMessage}</Alert>
          }
        </div>
        <div className="d-grid">
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </div>
      </form>
    </Container>
  )
}

export default Login

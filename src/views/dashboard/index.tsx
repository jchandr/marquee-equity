import React, { useEffect, type ReactElement, useState } from 'react'
import './Dashboard.css'
import { Col, Container, Row, Button, Modal, Form } from 'react-bootstrap'
import { useAppDispatch, useAppSelector } from '../../store/hooks'
import { getTodos } from '../../mockBackend'
import { createNewTask, setTasks, toggleTaskDone } from '../../store/slices/appSlice'
import { Check, PlusSquare } from 'react-bootstrap-icons'

const DashboardIndex = (): ReactElement => {
  const tasks = useAppSelector(state => state.app.tasks)
  const user = useAppSelector(state => state.user)
  const dispatchAction = useAppDispatch()
  const [isNewTaskModalOpen, setIsNewTaskModalOpen] = useState(false)
  const [tempTaskDescription, setTempTaskDescription] = useState('')
  const [rootTaskId, setRootTaskId] = useState(null)

  const handleSubTaskCreate = (id: any): void => {
    setRootTaskId(id)
    setIsNewTaskModalOpen(true)
  }
  const buildTaskComponent = (rootId: any = null): ReactElement[] => {
    const filteredTasks = tasks.filter(({ rootId: tempId }) => tempId === rootId)
    const tasksToRender = filteredTasks.map(({ id, rootId, description, hasSubtasks, isDone }, index) => {
      return (
        <>
          <Row style={{ marginTop: 20, borderRadius: 3, minHeight: 25 }}>
            <Row>
              <Col style={{ verticalAlign: 'center', flexGrow: 1 }}>{description}</Col>
              <Button onClick={() => dispatchAction(toggleTaskDone({ id }))} variant={isDone === false ? 'outline-success' : 'success'} style={{ width: 50, height: 40, position: 'absolute', right: 60, justifyContent: 'center', flexDirection: 'column' }}>
                {isDone === true && <Check size={30}></Check>}
                </Button>
              <Button variant='danger' style={{ width: 50, height: 40, position: 'absolute', right: 5, justifyContent: 'center', flexDirection: 'column' }}>
                <PlusSquare onClick={() => { handleSubTaskCreate(id) }} size={25}></PlusSquare>
              </Button>
            </Row>
            <Row style={{ marginLeft: 5, flexDirection: 'column', paddingRight: 0 }}>
                <Col>
                {
                  buildTaskComponent(id)
                }
                </Col>
            </Row>
          </Row>
        </>
      )
    })
    return tasksToRender
  }

  const handleCreateModalClose = (): void => {
    setIsNewTaskModalOpen(false)
    setTempTaskDescription('')
    setRootTaskId(null)
  }

  const handleCreateTask = (): void => {
    dispatchAction(createNewTask({
      description: tempTaskDescription,
      rootId: rootTaskId,
      userId: user.id
    }))
    setIsNewTaskModalOpen(false)
    setTempTaskDescription('')
  }

  useEffect(() => {
    getTodos(user.token).then((data) => {
      dispatchAction(setTasks(data))
    }).catch(() => {
    })
  }, [])

  return (
    <Container className='p-3' fluid style={{ flex: 1 }} >
      <Modal show={isNewTaskModalOpen} onHide={handleCreateModalClose}>
        <Modal.Header closeButton>
          <Modal.Title>Create Task</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Form.Label htmlFor="inputPassword5">Task Description</Form.Label>
      <Form.Control
        id="inputPassword5"
        aria-describedby="passwordHelpBlock"
        onChange={(e: any) => { setTempTaskDescription(e.target.value) }}
        value={tempTaskDescription}
        autoFocus
      />
        </Modal.Body>
        <Modal.Footer>

          <Button variant="primary" onClick={handleCreateTask}>
            Save
          </Button>
        </Modal.Footer>
      </Modal>
      <Row>
        <h3 style={{ textAlign: 'center' }}>Task List</h3>
        <Container style={{ backgroundColor: 'beige', paddingTop: 15, paddingBottom: 30 }}>
          {buildTaskComponent()}
          </Container>
      </Row>
      <Row>
      <Button onClick={() => { setIsNewTaskModalOpen(true) }} variant='danger' style={{ justifyContent: 'center', flexDirection: 'column', alignItems: 'center' }}>
                <PlusSquare size={25}></PlusSquare>
              </Button>
      </Row>
    </Container>
  )
}

export default DashboardIndex

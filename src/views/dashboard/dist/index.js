"use strict";
exports.__esModule = true;
var react_1 = require("react");
require("./Dashboard.css");
var react_bootstrap_1 = require("react-bootstrap");
var hooks_1 = require("../../store/hooks");
var mockBackend_1 = require("../../mockBackend");
var appSlice_1 = require("../../store/slices/appSlice");
var DashboardIndex = function () {
    var tasks = hooks_1.useAppSelector(function (state) { return state.app.tasks; });
    var user = hooks_1.useAppSelector(function (state) { return state.user; });
    var dispatchAction = hooks_1.useAppDispatch();
    var buildTaskComponent = function (rootId) {
        if (rootId === void 0) { rootId = null; }
        var filteredTasks = tasks.filter(function (_a) {
            var tempId = _a.rootId;
            return tempId === rootId;
        });
        var tasksToRender = filteredTasks.map(function (_a) {
            var id = _a.id, rootId = _a.rootId, description = _a.description, hasSubtasks = _a.hasSubtasks;
            return (react_1["default"].createElement(react_1["default"].Fragment, null,
                react_1["default"].createElement(react_bootstrap_1.Row, { style: { marginTop: 10, borderRadius: 3 } },
                    react_1["default"].createElement(react_bootstrap_1.Row, null,
                        react_1["default"].createElement(react_bootstrap_1.Col, null, description),
                        react_1["default"].createElement(react_bootstrap_1.Col, null,
                            react_1["default"].createElement(react_bootstrap_1.Button, { variant: 'danger', style: { width: 30, height: 30, position: 'absolute', right: 5 } }))),
                    react_1["default"].createElement(react_bootstrap_1.Col, null,
                        react_1["default"].createElement(react_bootstrap_1.Button, { variant: 'success', style: { width: 30, height: 30, position: 'absolute', right: 40 } })),
                    react_1["default"].createElement(react_bootstrap_1.Row, { style: { marginLeft: 5 } }, buildTaskComponent(id)))));
        });
        return tasksToRender;
    };
    react_1.useEffect(function () {
        mockBackend_1.getTodos(user.token).then(function (data) {
            dispatchAction(appSlice_1.setTasks(data));
        })["catch"](function () {
        });
    }, []);
    return (react_1["default"].createElement(react_bootstrap_1.Container, { className: 'p-3', fluid: true, style: { flex: 1, backgroundColor: 'beige' } },
        react_1["default"].createElement(react_bootstrap_1.Row, { style: { textAlign: 'center' } },
            react_1["default"].createElement("h3", null, "ToDo App")),
        react_1["default"].createElement(react_bootstrap_1.Row, null,
            react_1["default"].createElement(react_bootstrap_1.Col, null,
                react_1["default"].createElement(react_bootstrap_1.Form.Label, { htmlFor: "inputPassword5" }, "Describe Your Task"),
                react_1["default"].createElement(react_bootstrap_1.Form.Control, { type: "text", id: "inputPassword5", "aria-describedby": "passwordHelpBlock" })),
            react_1["default"].createElement(react_bootstrap_1.Col, null,
                react_1["default"].createElement(react_bootstrap_1.Form.Label, { htmlFor: "inputPassword5" }, "SubTask of"),
                react_1["default"].createElement(react_bootstrap_1.Form.Select, { "aria-label": "SubTask of" },
                    react_1["default"].createElement("option", null, "Open this select menu"),
                    react_1["default"].createElement("option", { value: "1" }, "One"),
                    react_1["default"].createElement("option", { value: "2" }, "Two"),
                    react_1["default"].createElement("option", { value: "3" }, "Three")))),
        react_1["default"].createElement(react_bootstrap_1.Row, null,
            react_1["default"].createElement("h3", { className: 'mt-3', style: { textAlign: 'center' } }, "Task List"),
            react_1["default"].createElement(react_bootstrap_1.Container, null, buildTaskComponent()))));
};
exports["default"] = DashboardIndex;

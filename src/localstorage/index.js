import { useState } from 'react'

export const useLocalStorage = (keyName, defaultValue) => {
  const [storedValue, setStoredValue] = useState(() => {
    try {
      const value = localStorage.getItem(keyName)
      if (value) {
        return JSON.parse(value)
      }
      localStorage.setItem(keyName, JSON.stringify(defaultValue))
      return defaultValue
    } catch (err) {
      return defaultValue
    }
  })
  const setValue = (newValue) => {
    setStoredValue(newValue)
    localStorage.setItem(keyName, JSON.stringify(newValue))
  }
  return [storedValue, setValue]
}
